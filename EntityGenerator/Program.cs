﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var tableList = new List<string>();
            Console.Write("Enter Table Name(Enter END after typing all tables): ");
            do
            {
                var tableName = Console.ReadLine();
                if (string.IsNullOrWhiteSpace(tableName) || tableName.ToUpper() == "END")
                {
                    break;
                }
                else
                {
                    tableList.Add(tableName);
                }
            } while (true);

            Console.Write("Enter output path(press enter if you wanna create in same directory): ");
            var path = Console.ReadLine();

            GenerateMultipleTables(tableList, path);

            Console.ReadLine();
        }

        static void GenerateEntityFile(string path, string tableName, List<DBColumnInfo> columnInfoList)
        {
            var text = new StringBuilder();

            text.AppendLine("using System;");
            text.AppendLine("using System.Collections.Generic;");
            text.AppendLine("using System.Linq;");
            text.AppendLine("using System.Text;");
            text.AppendLine("using System.Threading.Tasks;");

            text.AppendLine("");
            text.AppendLine("namespace E21.Core.Domain.Tables");
            text.AppendLine("{");
            text.AppendLine("public class " + tableName + " : BaseEntity");
            text.AppendLine("    {");

            foreach (var columninfo in columnInfoList)
            {
                var line = "        public " + columninfo.GetDataType() + " " + columninfo.ColumnName + " { get; set; }";
                text.AppendLine(line);
            }

            text.AppendLine("    }");
            text.AppendLine("}");

            // take the app directory if path is empty
            if (string.IsNullOrWhiteSpace(path)) path = Directory.GetCurrentDirectory();

            // if given path does not exist then create directory
            if(!Directory.Exists(path)) Directory.CreateDirectory(path);
            var filePath = path + "\\" + tableName + ".cs";
            // var file = File.Open(filePath, FileMode.Create);

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.Write(text.ToString());
                writer.Flush();
                writer.Close();
            }
        }

        static void GenerateMapFile(string path, string tableName, List<DBColumnInfo> columnInfoList, List<string> keyList)
        {
            var text = new StringBuilder();

            text.AppendLine("using System;");
            text.AppendLine("using System.Collections.Generic;");
            text.AppendLine("using System.Linq;");
            text.AppendLine("using System.Text;");
            text.AppendLine("using System.Threading.Tasks;");
            text.AppendLine("using E21.Core.Domain.Tables;");
            text.AppendLine("using System.ComponentModel.DataAnnotations.Schema;");

            text.AppendLine("");
            text.AppendLine("namespace E21.Data.Mapping.Tables");
            text.AppendLine("{");
            text.AppendLine("public class " + tableName + "Map : NopEntityTypeConfiguration<" + tableName + ">");
            text.AppendLine("    {");
            text.AppendLine("        public " + tableName + "Map()");
            text.AppendLine("        {");
            text.AppendLine("            this.ToTable(\"" + tableName.ToLower() + "\");");

            if (keyList.Count == 0)
            {
                Console.WriteLine("The table " + tableName + " has no unique columns. Please insert it manually");
            }

            StringBuilder keyString = new StringBuilder("            this.HasKey(c => ");
            if (keyList.Count == 1)
            {
                keyString.Append("c." + keyList.First());
            }
            else
            {
                keyString.Append("new { ");
                foreach (var key in keyList)
                {
                    keyString.Append("c." + key);
                    if (key != keyList.Last())
                    {
                        keyString.Append(", ");
                    }
                }

                keyString.Append(" }");
            }

            keyString.Append(");");
            text.AppendLine(keyString.ToString());
            text.AppendLine("");



            foreach (var columninfo in columnInfoList)
            {
                var line = "            this.Property(u => u." + columninfo.ColumnName + ").HasColumnName(\""
                    + columninfo.ColumnName + "\");";
                text.AppendLine(line);
            }

            text.AppendLine("        }");
            text.AppendLine("    }");
            text.AppendLine("}");

            // take the app directory if path is empty
            if (string.IsNullOrWhiteSpace(path)) path = Directory.GetCurrentDirectory();

            // if given path does not exist then create directory
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
            var filePath = path + "\\" + tableName + "Map.cs";
            // var file = File.Open(filePath, FileMode.Create);

            using (StreamWriter writer = new StreamWriter(filePath))
            {
                writer.Write(text.ToString());
                writer.Flush();
                writer.Close();
            }
        }

        static void GenerateSingleFile(string tableName, string path)
        {
            var reader = new ColumnInfoReader();
            var columnInfoList = reader.GetColumnInfos(tableName);
            if (columnInfoList.Count == 0)
            {
                Console.WriteLine(tableName + " does not exist");
                return;
            }

            GenerateEntityFile(path, tableName, columnInfoList);
            Console.WriteLine("EntityFile for " + tableName +  " has been created");

            GenerateMapFile(path, tableName, columnInfoList, reader.GetUniqueConstraintFields(tableName));
            Console.WriteLine("MapFile for " + tableName + " has been created");
        }

        static void GenerateMultipleTables(List<string> tableList, string path)
        {
            foreach (var table in tableList)
            {
                GenerateSingleFile(table, path);
            }
        }
    }
}
