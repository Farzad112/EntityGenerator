﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityGenerator
{
    public class ColumnInfoReader
    {
        #region Properties
        private readonly string _sqlConnectionString;
        #endregion

        #region Ctor
        public ColumnInfoReader()
        {
            _sqlConnectionString = "Data Source=.\\SQLEXPRESS;Initial Catalog=demo90;Persist Security Info=False;User ID=sa;Password=Enosis123";
        }
        #endregion

        #region Public Methods
        public List<DBColumnInfo> GetColumnInfos(string tableName)
        {
            var columnInfoList = new List<DBColumnInfo>();

            try
            {
                using (SqlConnection conn = new SqlConnection())
                {
                    conn.ConnectionString = _sqlConnectionString;
                    conn.Open();

                    SqlCommand command = new SqlCommand("SELECT Column_name, Data_Type, Is_Nullable FROM demo90.INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" +
                        tableName + "'", conn);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            columnInfoList.Add(new DBColumnInfo
                            {
                                ColumnName = reader[0].ToString(),
                                DataType = reader[1].ToString(),
                                IsNullable = reader[2].ToString() == "YES"
                            });
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return columnInfoList;
        }

        public List<string> GetUniqueConstraintFields(string tableName)
        {
            var fieldList = new List<string>();

            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = _sqlConnectionString;
                conn.Open();

                SqlCommand command = new SqlCommand(@"SELECT
                             COLUMN_NAME_UNIQUE = col.name
                        FROM
                         sys.indexes ind
                        INNER JOIN
                             sys.index_columns ic ON  ind.object_id = ic.object_id and ind.index_id = ic.index_id
                        INNER JOIN
                             sys.columns col ON ic.object_id = col.object_id and ic.column_id = col.column_id
                        INNER JOIN
                             sys.tables t ON ind.object_id = t.object_id
                        WHERE
                          t.name = '" + tableName + @"'
                             AND ind.is_unique = 1", conn);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        fieldList.Add(reader[0].ToString());
                    }
                }
            }

            return fieldList;
        }
        #endregion

    }

    public class DBColumnInfo
    {
        private string _columnName;
        public string ColumnName
        {
            get
            {
                if (_columnName == "location" || _columnName == "status")
                {
                    return _columnName + "x";
                }
                else
                {
                    return _columnName;
                }
            }
            set
            {
                _columnName = value;
            }
        }
        public string DataType { get; set; }
        public bool IsNullable { get; set; }

        public string GetDataType()
        {
            var type = "";
            switch (DataType)
            {
                case "int":
                    type = "int";
                    break;

                case "numeric": // not sure
                case "decimal":
                    type = "decimal";
                    break;

                case "datetime":
                case "timestamp":
                    type = "DateTime";
                    break;

                case "smallint":
                    type = "short";
                    break;

                case "char": // not sure
                case "varchar": // not sure
                case "nvarchar":
                    type = "string";
                    break;

                case "float":
                    type = "double";
                    break;

                case "bigint":
                    type = "long";
                    break;

                case "bit": // not sure
                    type = "bool";
                    break;
            }

            if (IsNullable && type != "string")
            {
                type += "?";
            }

            return type;
        }
    }
}
